package com.atlassian.plugin.spring.scanner.annotation.export;

/**
 * Denotes a single entry in the service property {@link java.util.Map} that is added to the OSGi Service Registry.
 * <p>
 * Multiple properties can be declared in any class annotated by {@link ExportAsService}, {@link ExportAsDevService} or
 * {@link ModuleType}.
 * <p>
 * For every ServiceProperty defined, an entry will be added to the service property map in the OSGi registry.
 * @since 2.2.0
 */
public @interface ServiceProperty {
    /**
     * The key to be put into the service registry map.
     * @return the key to use
     */
    String key();

    /**
     * The value to be put into the service registry map.
     * @return the value corresponding to the key.
     */
    String value();
}
