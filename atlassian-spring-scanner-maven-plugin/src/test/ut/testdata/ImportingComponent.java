package ut.testdata;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;

@Component
public class ImportingComponent
{
    @ComponentImport
    private DeclaredComponent declaringComponent;
}
