package com.atlassian.plugin.spring.scanner.runtime.impl;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

import java.beans.PropertyDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Dictionary;
import java.util.Enumeration;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * A BeanPostProcessor that logs information about the bean before/after initialisation,
 * so that developers can debug their Spring context.
 */
public class DevModeBeanInitialisationLoggerBeanPostProcessor
        implements InstantiationAwareBeanPostProcessor, InitializingBean, DestructionAwareBeanPostProcessor, DisposableBean {

    /**
     * Setting this system property to <code>"true"</code> enables per-bundle WARN logging by this class.
     * Developers who are not interested in the loading/unloading of a given bundle can set that bundle's
     * spring scanner logging to ERROR.
     */
    public static final String ATLASSIAN_DEV_MODE = "atlassian.dev.mode";

    private static final String BUNDLE_LOGGER_NAME_FORMAT = "com.atlassian.plugin.spring.scanner.%s";

    private final Bundle bundle;
    private final Logger bundleLogger;

    public DevModeBeanInitialisationLoggerBeanPostProcessor(final BundleContext bundleContext) {
        this.bundle = requireNonNull(bundleContext.getBundle());
        this.bundleLogger = getLogger(format(BUNDLE_LOGGER_NAME_FORMAT, bundleContext.getBundle().getSymbolicName()));
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        logBeanDetail("AfterInitialisation", bean.getClass(), beanName);
        return bean;
    }

    @Override
    public Object postProcessBeforeInstantiation(Class beanClass, String beanName) {
        logBeanDetail("BeforeInstantiation", beanClass, beanName);
        return null; // do default instantiation
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) {
        return true; // don't skip property setting
    }

    @Override
    public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) {
        return pvs; // pass values to be set through
    }

    @Override
    public void postProcessBeforeDestruction(final Object bean, final String beanName) throws BeansException {
        logBeanDetail("BeforeDestruction", bean.getClass(), beanName);
    }

    private void logBeanDetail(String stage, Class beanClass, String beanName) {
        bundleLogger.debug("{} [bean={}, type={}]", stage, beanName, beanClass.getName());
    }

    @Override
    public void afterPropertiesSet() {
        // only log the startup info once, the first time we are hit
        logInDevMode("Spring context started for bundle: {} id({}) v({}) {}",
                bundle.getSymbolicName(), bundle.getBundleId(), bundle.getVersion(), bundle.getLocation());
        if (bundleLogger.isTraceEnabled()) {
            final StringWriter sw = new StringWriter();
            final PrintWriter out = new PrintWriter(sw);

            out.format("\tBundle Headers :\n");
            final Dictionary headers = bundle.getHeaders();
            final Enumeration keys = headers.keys();
            while (keys.hasMoreElements()) {
                final Object key = keys.nextElement();
                final Object value = headers.get(key);
                out.format("\t\t%s: %s\n", key, value);
            }

            bundleLogger.trace(sw.toString());
        }
    }

    @Override
    public void destroy() {
        logInDevMode("Spring context destroyed for bundle: {} id({}) v({})",
                bundle.getSymbolicName(), bundle.getBundleId(), bundle.getVersion());
    }

    private void logInDevMode(final String message, final Object... arguments) {
        if (isDevMode()) {
            // This is WARN because in JIRA at least, that's the level of the root logger,
            // and we want these messages to be shown by default (in dev mode).
            bundleLogger.warn(message, arguments);
        }
    }

    private static boolean isDevMode() {
        return Boolean.getBoolean(ATLASSIAN_DEV_MODE);
    }
}
