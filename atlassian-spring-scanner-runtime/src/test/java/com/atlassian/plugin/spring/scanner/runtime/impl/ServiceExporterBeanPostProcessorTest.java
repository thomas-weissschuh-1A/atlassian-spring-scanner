package com.atlassian.plugin.spring.scanner.runtime.impl;

import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PrivateService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicModuleTypeWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicModuleTypeWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicNonInterfaceService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicNonInterfaceServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.Service;
import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

import static com.atlassian.plugin.spring.scanner.runtime.impl.IsAopProxy.aopProxy;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExporterBeanPostProcessorTest {
    @Mock
    private Bundle bundle;
    @Mock
    private BundleContext bundleContext;
    @Mock
    private ConfigurableListableBeanFactory beanFactory;
    @Mock
    private ExportedSeviceManager exportedSeviceManager;

    private ServiceExporterBeanPostProcessor exporterPostProcessor;
    private ApplicationContext applicationContext;

    @Before
    public void setUp() throws Exception {
        when(bundle.getBundleContext()).thenReturn(bundleContext);
        when(bundleContext.getBundle()).thenReturn(bundle);

        System.setProperty(ServiceExporterBeanPostProcessor.ATLASSIAN_DEV_MODE_PROP, Boolean.TRUE.toString());
        this.exporterPostProcessor = new ServiceExporterBeanPostProcessor(bundleContext, beanFactory, exportedSeviceManager);
        this.exporterPostProcessor.afterPropertiesSet();

        this.applicationContext = new ClassPathXmlApplicationContext("classpath:/com/atlassian/plugin/spring/scanner/runtime/impl/testservices/testservicesContext.xml");
    }

    @Test
    public void testNonProxyExportAsService() throws Exception {
        testNonProxyBeanIsExported(PublicService.class, Service.class);
    }

    @Test
    public void testNonProxyExportAsDevService() throws Exception {
        testNonProxyBeanIsExported(PublicDevService.class, Service.class);
    }

    @Test
    public void testNonProxyNonInterfaceExportAsService() throws Exception {
        testNonProxyBeanIsExported(PublicNonInterfaceService.class, PublicNonInterfaceService.class);
    }

    @Test
    public void testProxyExportAsService() throws Exception {
        testProxyBeanIsExported(PublicServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyExportAsDevService() throws Exception {
        testProxyBeanIsExported(PublicDevServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyNonInterfaceExportAsService() throws Exception {
        testProxyBeanIsExported(PublicNonInterfaceServiceProxy.class, PublicNonInterfaceServiceProxy.class);
    }

    @Test
    public void testPrivateServiceNotExported() throws Exception {
        testBeanIsExported(PrivateService.class, CoreMatchers.any(Object.class), CoreMatchers.any(Class.class), false);
    }

    @Test
    public void testPublicServiceExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(PublicServiceWithProperty.class, "service_key", "service_with_properties_key");
    }

    @Test
    public void testPublicModuleTypeExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(PublicModuleTypeWithProperty.class, "module_key", "module_type_with_properties_key");
    }

    @Test
    public void testPublicDevServiceExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(PublicDevServiceWithProperty.class, "dev_key", "dev_service_with_properties_key");
    }

    @Test
    public void testPublicServiceExportedWithServiceProperties() throws Exception {
        Map properties = getPropertiesFromExportedService(PublicServiceWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("service_key_1"), equalTo("service_value_1"));
        assertThat(properties.get("service_key_2"), equalTo("service_value_2"));
    }

    @Test
    public void testPublicDevServiceExportedWithServiceProperties() throws Exception {
        Map properties = getPropertiesFromExportedService(PublicDevServiceWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("dev_key_1"), equalTo("dev_value_1"));
        assertThat(properties.get("dev_key_2"), equalTo("dev_value_2"));
    }

    @Test
    public void testPublicModuleTypeExportedWithServiceProperties() throws Exception {
        Map properties = getPropertiesFromExportedService(PublicModuleTypeWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("module_key_1"), equalTo("module_value_1"));
        assertThat(properties.get("module_key_2"), equalTo("module_value_2"));
    }

    private Map getPropertiesFromExportedService(Class serviceClass) throws Exception {
        Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(serviceClass);
        assertThat(nameBeanEntry.getValue(), CoreMatchers.any(Object.class));
        ArgumentCaptor<Map> propertiesCaptor = ArgumentCaptor.forClass(Map.class);

        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedSeviceManager).registerService(
                any(BundleContext.class),
                eq(nameBeanEntry.getValue()),
                eq(nameBeanEntry.getKey()),
                propertiesCaptor.capture(),
                argThat(CoreMatchers.any(Class.class)));

        Map properties = propertiesCaptor.getValue();
        return properties;
    }

    private void testBeanIsExportedWithServiceProperty(Class serviceClass, String key, String expectedProperty) throws Exception {
        Map properties = getPropertiesFromExportedService(serviceClass);
        assertThat(properties.size(), equalTo(1));
        assertThat(properties.get(key), equalTo(expectedProperty));
    }

    private void testBeanIsExported(Class<?> beanImplClass, Matcher<Object> beanMatcher, Matcher<Class> interfaceMatcher, boolean expectExported) throws Exception {
        Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(beanImplClass);
        assertThat(nameBeanEntry.getValue(), beanMatcher);
        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedSeviceManager, expectExported ? times(1) : never()).registerService(
                any(BundleContext.class),
                eq(nameBeanEntry.getValue()),
                eq(nameBeanEntry.getKey()),
                anyMap(),
                argThat(interfaceMatcher));
    }

    private void testNonProxyBeanIsExported(Class<?> beanImplClass, Class expectInstanceOf) throws Exception {
        testBeanIsExported(beanImplClass, not(aopProxy()), equalTo(expectInstanceOf), true);
    }

    private void testProxyBeanIsExported(Class<?> beanImplClass, Class expectInstanceOf) throws Exception {
        testBeanIsExported(beanImplClass, aopProxy(), equalTo(expectInstanceOf), true);
    }

    protected Map.Entry<String, Object> getBeanByImplClass(Class<?> beanImplClass) {
        final String beanName = getBeanDefaultName(beanImplClass);
        final Object bean = applicationContext.getBean(beanName);
        assertThat(bean, CoreMatchers.notNullValue());
        return new DefaultMapEntry(beanName, bean);
    }

    private String getBeanDefaultName(Class<?> type) {
        String name = type.getSimpleName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

}