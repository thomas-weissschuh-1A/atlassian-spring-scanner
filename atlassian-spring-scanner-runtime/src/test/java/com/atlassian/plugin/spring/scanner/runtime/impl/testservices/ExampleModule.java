package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

/**
 * This is the interface used to register ModuleType implementations with OSGi.
 * This is used for testing purposes only.
 */
public interface ExampleModule {
}
