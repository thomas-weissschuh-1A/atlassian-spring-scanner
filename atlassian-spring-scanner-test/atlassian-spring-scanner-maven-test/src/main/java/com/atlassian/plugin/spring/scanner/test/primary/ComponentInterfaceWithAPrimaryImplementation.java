package com.atlassian.plugin.spring.scanner.test.primary;

/**
 * This interface will have multiple implementations. One is marked with @Primary and so should be selected when this
 * interface is autowired.
 */
interface ComponentInterfaceWithAPrimaryImplementation {
}
