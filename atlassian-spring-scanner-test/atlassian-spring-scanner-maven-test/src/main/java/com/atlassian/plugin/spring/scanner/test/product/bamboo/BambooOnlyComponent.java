package com.atlassian.plugin.spring.scanner.test.product.bamboo;

import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A component that's only instantiated when running in Bamboo
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@BambooComponent
public class BambooOnlyComponent {

    private final BuildExecutionManager buildExecutionManager;

    @Autowired
    public BambooOnlyComponent(@BambooImport BuildExecutionManager buildExecutionManager) {
        this.buildExecutionManager = buildExecutionManager;
    }
}
