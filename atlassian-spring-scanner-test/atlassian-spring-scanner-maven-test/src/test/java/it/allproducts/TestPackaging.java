package it.allproducts;

import com.atlassian.plugin.spring.scanner.test.InternalComponent;
import it.allproducts.ComponentExpectations.AbstractExpectedComponent;
import com.google.common.io.CharStreams;
import it.perproduct.AbstractComponentsInProductTest;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

/**
 * Check that the index files produced by atlassian-spring-scanner-maven-plugin are what we expect given the annotations in this plugin.
 * <p>
 * Note this test is <b>build-time only</b> - it just validates the packaging part of the scanner.
 * It's in this integration-test phase due to Maven classpath issues.
 * It is not asserting anything about the plugin running in the product - it's actually just looking at
 * either your {@code /target/classes} directory (if run from IDEA) or maybe the artifact jar on the classpath (from Maven).
 *
 * @see AbstractComponentsInProductTest subclasses for runtime tests
 */
public class TestPackaging {
    @Test
    public void testExpectedComponents() throws Exception {
        assertIndexContents("component",
                ComponentExpectations.COMMON.getExpectedScannerCreatedComponents());
    }

    @Test
    public void testExpectedImports() throws Exception {
        assertIndexContents("imports",
                ComponentExpectations.COMMON.getExpectedScannerCreatedComponentImports());
    }

    @Test
    public void testExpectedExports() throws Exception {
        assertIndexContents("exports",
                ComponentExpectations.COMMON.getExpectedScannerCreatedExports());
    }

    @Test
    public void testExpectedDevExports() throws Exception {
        assertIndexContents("dev-exports",
                ComponentExpectations.COMMON.getExpectedScannerCreatedDevExports());
    }

    @Test
    public void testExpectedDynamicComponents() throws Exception {
        assertIndexContents("profile-dynamic/component",
                ComponentExpectations.COMMON.getExpectedScannerCreatedDynamicComponents());
    }

    @Test
    public void testExpectedDynamicImports() throws Exception {
        assertIndexContents("profile-dynamic/imports",
                ComponentExpectations.COMMON.getExpectedScannerCreatedDynamicComponentImports());
    }

    @Test
    public void testExpectedPackageInfoComponents() throws Exception {
        //
        // these two use package info level annotations
        assertIndexContents("profile-unused/component",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent1",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent2");

        //
        // this specific class has another profile override and hence does NOT take the package level ones
        //
        // It cumulative inheritance, its override specifics
        //
        assertIndexContents("profile-specific-override/component",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedSpecificOverride");
    }

    @Test
    public void testBambooComponents() throws Exception {
        testProductSpecificComponents("bamboo", ComponentExpectations.BAMBOO);
    }

    @Test
    public void testBitbucketComponents() throws Exception {
        testProductSpecificComponents("bitbucket", ComponentExpectations.BITBUCKET);
    }

    @Test
    public void testConfluenceComponents() throws Exception {
        testProductSpecificComponents("confluence", ComponentExpectations.CONFLUENCE);
    }

    @Test
    public void testFecruComponents() throws Exception {
        testProductSpecificComponents("fecru", ComponentExpectations.FECRU);
    }

    @Test
    public void testJiraComponents() throws Exception {
        testProductSpecificComponents("jira", ComponentExpectations.JIRA_CLOUD);
    }

    @Test
    public void testRefappComponents() throws Exception {
        testProductSpecificComponents("refapp", ComponentExpectations.REFAPP);
    }

    @Test
    public void testStashComponents() throws Exception {
        testProductSpecificComponents("stash", ComponentExpectations.STASH);
    }

    private void testProductSpecificComponents(String indexFileQualifier, ComponentExpectations productSpecificExpectations) throws IOException {
        assertIndexContents("component-" + indexFileQualifier,
                productSpecificExpectations.getProductSpecificScannerCreatedComponents());
        assertIndexContents("imports-" + indexFileQualifier,
                productSpecificExpectations.getProductSpecificScannerCreatedComponentImports());
    }

    private void assertIndexContents(final String indexFilename, final List<? extends AbstractExpectedComponent> expectedItems)
            throws IOException {
        final List<String> actual = readIndexFile(indexFilename);
        Collection<Matcher<? super String>> lineMatchers =
                expectedItems.stream()
                        .map(AbstractExpectedComponent::getIndexLineMatcher)
                        .collect(Collectors.toList());
        assertThat(actual, containsInAnyOrder(lineMatchers));
    }

    private void assertIndexContents(final String indexFilename, final String... expected)
            throws IOException {
        final List<String> actual = readIndexFile(indexFilename);
        assertThat(actual, containsInAnyOrder(expected));
    }

    private List<String> readIndexFile(final String indexFileName) throws IOException {
        final String resourcePath = "META-INF/plugin-components/" + indexFileName;

        // A class inside the plugin
        final InputStream inputStream = InternalComponent.class.getClassLoader().getResourceAsStream(resourcePath);

        assertThat("Unable to read expected index file '" + resourcePath + "'", inputStream, notNullValue());

        try (InputStreamReader reader = new InputStreamReader(inputStream)) {
            return CharStreams.readLines(reader);
        }
    }
}
