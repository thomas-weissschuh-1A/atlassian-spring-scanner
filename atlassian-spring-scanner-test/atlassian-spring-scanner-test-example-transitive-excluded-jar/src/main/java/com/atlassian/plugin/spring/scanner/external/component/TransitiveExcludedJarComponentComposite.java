package com.atlassian.plugin.spring.scanner.external.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is a component that is transitive but excluded in maven config
 */
@Component
public class TransitiveExcludedJarComponentComposite {
    @Autowired
    TransitiveExcludedJarComponent1 externalJarComponent1;

    @Autowired
    TransitiveExcludedJarComponent2 externalJarComponent2;
}
